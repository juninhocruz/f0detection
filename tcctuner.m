function varargout = tcctuner(varargin)
% TCCTUNER MATLAB code for tcctuner.fig
%      TCCTUNER, by itself, creates a new TCCTUNER or raises the existing
%      singleton*.
%
%      H = TCCTUNER returns the handle to a new TCCTUNER or the handle to
%      the existing singleton*.
%
%      TCCTUNER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TCCTUNER.M with the given input arguments.
%
%      TCCTUNER('Property','Value',...) creates a new TCCTUNER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before tcctuner_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to tcctuner_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help tcctuner

% Last Modified by GUIDE v2.5 21-May-2018 06:31:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tcctuner_OpeningFcn, ...
                   'gui_OutputFcn',  @tcctuner_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before tcctuner is made visible.
function tcctuner_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tcctuner (see VARARGIN)

% Choose default command line output for tcctuner
handles.output = hObject;
handles.isRunning = false;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes tcctuner wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = tcctuner_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)
% hObject    handle to startButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% FOR DEBUG
%app = {};
%app.handles = handles;
%app.guidata = @guidata;
%threadTask(app, 44000, 0.5, 440)

% CODE
global stopped;
if(handles.isRunning)
    hObject.String = 'Start';
    handles.isRunning = false;
    stopped = true;
else
    hObject.String = 'Stop';
    handles.isRunning = true;
    stopped = false;
    startTuner(handles, @guidata);
end
guidata(hObject, handles);

% --- Executes on selection change in frefMenu.
function frefMenu_Callback(hObject, eventdata, handles)
% hObject    handle to frefMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns frefMenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from frefMenu


% --- Executes during object creation, after setting all properties.
function frefMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frefMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
frefs = {432, 434, 436, 438, 440, 442, 444, 446};
hObject.String = frefs;
hObject.Value = 5;
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
