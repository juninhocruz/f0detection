function pitch = threadTask(app, Fs, recTime, fref)
    handles = app.handles;
    guidata = app.guidata;
    
    % Grava T
    T = 1/Fs;
    nBits = 24;
    nChannels = 1;
    
    recorder = audiorecorder(Fs, nBits, nChannels);
    
    disp('recording...');
    recordblocking(recorder, recTime);
    disp('recorded!');
    
    data = getaudiodata(recorder);
    
    % Processa
    [pitch, f, P] = detectPitch(data, Fs, nChannels, 80, 490);
    
    % Detecta nota
    [notename, octave, fperfect, fant, fpos] = nearNote(pitch, fref);
    %handles.

    % Envia resultados
    handles.pitchTV.String = ['Frequency: ', num2str(pitch,5), 'Hz'];
    handles.noteTV.String = notename;
    handles.fexpTV.String = [num2str(fperfect,5), 'Hz'];
    handles.octaveTV.String = int2str(octave);
    guidata(handles.pitchTV, handles);
    
    %plot(handles.axes, 1:10,fperfect*ones(1,10), '-m', 'LineWidth', 54);
    
    plot(handles.axes, 1:10,fperfect*ones(1,10), '-k', 'LineWidth', 6);
    hold on;
    if(abs(fperfect - pitch) < 3)
        format = '-g';
    else
        format = '-r';
    end
    plot(handles.axes, 1:10,pitch*ones(1,10), format, 'LineWidth', 12);
    hold off;
    handles.axes.YLim =[fperfect-30 fperfect+30];
end