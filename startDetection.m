function startDetection(app)
    Fs = 44100;
    T = 1/Fs;
    nBits = 24;
    nChannels = 2;
    timeForRec = 6;
    bpm = 60;
    windowSize = round((1/16)*(60/bpm)*Fs);
    recSize = Fs*timeForRec - mod(Fs*timeForRec, windowSize); % sample total that will be analysed
    
    setVal(app, 'Fs', Fs);
    setVal(app, 'T', T);
    setVal(app, 'nBits', nBits);
    setVal(app, 'nChannels', nChannels);
    setVal(app, 'timeForRec', timeForRec);
    setVal(app, 'bpm', bpm);
    setVal(app, 'windowSize', windowSize);
    setVal(app, 'recSize', recSize);
    
    recorder = audiorecorder(Fs,nBits,nChannels);
    
    disp('recording...');
    recordblocking(recorder, timeForRec);
    disp('recorded!');
    
    data = getaudiodata(recorder);
    %spectrograma = zeros(size(data,1)/2+1, recSize/windowSize);
    %f = zeros(size(data,1)/2+1);
    
    %audiowrite('samples/sample008.m4a',data,Fs);
    
    disp('Analizing...');
    cont = 0;
    for i = 1 : windowSize : recSize
        cont = 1+cont;
        disp('teste');
        window = data(i:i+(windowSize-1), 1);
        [pitch, f, P] = detectPitch(window, Fs, nChannels);
        disp(['janela[', int2str(cont), '] = ', int2str(size(window)), ' | i = ', int2str(i), ' | pitch = ', int2str(pitch), ' | mean = ', num2str(mean(window))]);
    %    spectrograma(:,((i-1)/windowSize)+1) = P';
    end
    disp('Analized!');

    sound(data, Fs, nBits);
end
