function startTuner(handles, guidata)
    app = {};
    app.handles = handles;
    app.guidata = guidata;
    handles.pitchTV.String = 'Ol� Mundo!';
    guidata(handles.pitchTV, handles);
    
    %% Configurações Iniciais Manuais
    % Forma de obtenção do audio: ['recorder', 'local']
    import_audio_mode = 'recorder';
    % Path + Nome do audio a ser manipulado
    audio_name = 'samples/sample009.ogg';
    % Tempo de gravação - Em caso de 'recorder'
    timeForRec = 6;
    % Frequência Máxima a ser detectada
    Fmax = 2000;
    % Batidas por minuto esperado para o audio
    bpm = 196;
    % Menor nota que deseja-se registrar
    minNote = 1; % semicolcheia
    % Frequ�ncia de Refer�ncia A4
    fref = str2num(handles.frefMenu.String{handles.frefMenu.Value});

    %% Obtenção do �?udio
    if(strcmp(import_audio_mode, 'local'))
        [audiofile, Fs, audioInfo] = getAudio(audio_name);

        T = 1/Fs;
        nBits = 24;
        nChannels = audioInfo.NumChannels;
        timeForRec = audioInfo.Duration;
        windowSize = round((minNote/2)*(60/bpm)*Fs);

        data = audiofile;
    else
        Fs = 44100;
        T = 1/Fs;
        nBits = 24;
        nChannels = 1;
        windowSize = round((minNote/2)*(60/bpm)*Fs);
        recSize = Fs*timeForRec - mod(Fs*timeForRec, windowSize); % sample total that will be analysed      
    end
    launchThreads(app, Fs, windowSize, fref)
end

function launchThreads(app, Fs, windowSize, fref)
    %p = gcp();
    % To request multiple evaluations, use a loop.
    
    %parfeval(p,@threadTask,1,app, Fs, windowSize/Fs, fref);
    global stopped;
    idx = 0;
    while(~stopped)
        threadTask(app, Fs, windowSize/Fs, fref);
        disp([num2str(idx), ' launched.']);
        idx = idx + windowSize/Fs;
    end
end