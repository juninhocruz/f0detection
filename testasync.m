p = gcp();
% To request multiple evaluations, use a loop.
for idx = 1:20
  f(idx) = parfeval(p,@threadTask,1,'nada', 44000, 0.5); % Square size determined by idx
  disp([int2str(idx), ' launched.']);
  pause(0.5),
end
% Collect the results as they become available.
results = cell(1,20);
for idx = 1:20
  % fetchNext blocks until next results are available.
  [completedIdx,value] = fetchNext(f);
  results{completedIdx} = value;
  %fprintf('Tempo: %d -.\n', completedIdx);
  disp(['tempo: ', num2str(completedIdx*0.5), ' | ', 'pitch: ', num2str(value)]);
end