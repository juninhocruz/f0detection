function startDetection(app)
    Fs = 44100;
    nBits = 24;
    nChannels = 2;
    timeForProcessing = 1/16;
    maxTimeForRec = 20;
    totalProcessing = maxTimeForRec / timeForProcessing;
    rec = zeros(nChannels, totalProcessing*maxTimeForRec);
    recorder = audiorecorder(Fs,nBits,nChannels);
    app.TempStorage('Fs') = Fs;
    app.TempStorage('nBits') = nBits;
    app.TempStorage('nChannels') = nChannels;
    app.TempStorage('timeForProcessing') = timeForProcessing;
    app.TempStorage('maxTimeForRec') = maxTimeForRec;
    app.TempStorage('totalProcessing') = totalProcessing;
    app.TempStorage('processingCount') = 0;
    app.TempStorage('rec') = rec;
    
    recCycle(app, recorder, timeForProcessing);
end

function recCycle(app, recorder, timeForProcessing)
    Fs = recorder.SampleRate;
    nBits = recorder.BitsPerSample;
    T = 1/Fs;
    disp('recording...');
    recordblocking(recorder, timeForProcessing);
    disp('recorded!');
    data = getaudiodata(recorder);
    plotTime(app, data, T, timeForProcessing)
    plotFFT(app, data(:,1)');
    sound(data, Fs, nBits);
    isDetecting = app.TempStorage('isDetecting');
    if(isDetecting == 1)
       recCycle(app, recorder, timeForProcessing); 
    end
end
