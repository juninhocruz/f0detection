function plotTime(app, data, T, timeForProcessing)
    processingCount = app.TempStorage('processingCount');
    totalProcessing = app.TempStorage('totalProcessing');
    
    if(processingCount < totalProcessing)
        rec = app.TempStorage('rec');
        rec(:,1+(processingCount*(timeForProcessing/T)):((processingCount+1)*(timeForProcessing/T))) = data';
        app.TempStorage('rec') = rec;
        processingCount = processingCount + 1;
        app.TempStorage('processingCount') = processingCount;
    end

    n = 0:T:timeForProcessing-T;
    plot(app.UIAxesTime, n, data(:,1));
end