function onClickToStartDetection(app, event)
    disp('Starting!');
    keys = {'objectName'};
    values = {'tempStorage'};
    app.TempStorage = containers.Map(keys, values,'UniformValues', false);
    app.TempStorage('isDetecting') = 1;
    startTestDetection(app);
end