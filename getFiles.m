function filesNames = getFiles(diretory)
    filesList = dir(diretory);
    listSize = size(filesList, 1);
    names = cell(1, listSize-2);
    for i=3:listSize
        names{i-2} = filesList(i).name;
    end
    filesNames = names;
end