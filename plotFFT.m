function plotFFT(app, data)
    Fs = app.TempStorage('Fs');
    nBits = app.TempStorage('nBits');
    nChannel = app.TempStorage('nChannels');
    timeForProcessing = app.TempStorage('timeForProcessing');
    T = 1/Fs;
    samplesPerProcessing = timeForProcessing/T;

    DATA = fft(data);

    P2 = abs(DATA/samplesPerProcessing);
    P1 = P2(1:samplesPerProcessing/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    
    f = Fs*(0:(samplesPerProcessing/2))/samplesPerProcessing;
    plot(app.UIAxesFreq, f,P1)
    app.UIAxesFreq.XLim = [0 1000];
    title('Single-Sided Amplitude Spectrum of X(t)')
    xlabel('f (Hz)')
    ylabel('|P1(f)|')
    Amax = max(P1);
    Imax = find(P1 == Amax);
    Fmax = f(Imax);
    app.UILabelPitch.Text = int2str(Fmax);
end