function [notename, octave, fperfect, fant, fpos] = nearNote(f, fref)
    frefs = openjsonfile('notes.json');
    try
        notes = frefs.(strcat('x', int2str(fref)));
    catch
        notes = frefs.x440;
    end
    note = {};
    note.note = 'X0';
    note.frequency = 0;
    for i=1:length(notes)
        if(notes(i).frequency > f)
            note = notes(i);
            if(i ~= 1)
                antnote = notes(i-1);
                middle = antnote.frequency + (note.frequency - antnote.frequency)/2;
                if(f < middle)
                    note = antnote;
                    if(i ~= 2)
                        fant = notes(i-2).frequency;
                    else
                        fant = 0;
                    end
                else
                    fant = notes(i-1).frequency;
                end
                
                if(i < length(notes))
                    fpos = notes(i+1).frequency;
                else
                    fpos = notes(i).frequency + 10;
                end
            else
                fpos = notes(1).frequency;
                fant = 0;
            end
            break;
        end
    end
    notename = note.note(1:end-1);
    octave = str2num(note.note(end));
    fperfect = note.frequency;
end