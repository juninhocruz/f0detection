function convertToMono(audioPath)
    [audiofile, Fs, ~] = getAudio(audioPath);
    [x, y] = size(audiofile);
    if(x > y)
        audiowrite(strcat(audioPath(1:end-3), 'm4a'), audiofile(:, 1), Fs);
    else
        audiowrite(strcat(audioPath(1:end-3), 'm4a'), audiofile(1, :), Fs);
    end
end