function [pitch, f, P] = detectPitch(data, Fs, nChannels, fminlim, fmaxlim)
    T = 1/Fs;
    samplesPerProcessing = size(data, 1);
    requiredSize = 2^16;
    data = [data; zeros(requiredSize-samplesPerProcessing,1)];
    samplesPerProcessing = requiredSize;

    DATA = fft(data);

    P2 = abs(DATA/samplesPerProcessing);
    P1 = P2(1:samplesPerProcessing/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    
    f = Fs*(0:(samplesPerProcessing/2))/samplesPerProcessing;
    %plot(app.UIAxesFreq, f,P1)
    %app.UIAxesFreq.XLim = [0 1000];
    %title('Single-Sided Amplitude Spectrum of X(t)')
    %xlabel('f (Hz)')
    %ylabel('|P1(f)|')
    
    Amax = max(P1(:));
    Imax = find(P1 == Amax);
    Fmax = f(Imax);
    pitch = Fmax;
    P = P1;
end