%% Projeto Final de Curso - Sistema de Detecção de Pitch
%
% Universidade Federal do Amazonas
% Faculdade de Tecnologia
% Engenharia da Computação
% Autor: António César Vieira da Cruz Júnior (CRUZ JR, A.C.V.)
% 

%% Configurações Iniciais Manuais
% Forma de obtenção do audio: ['recorder', 'local']
import_audio_mode = 'local';
% Path + Nome do audio a ser manipulado
audio_name = 'samples/piano04.ogg';
% Tempo de gravação - Em caso de 'recorder'
timeForRec = 6;
% Frequência Máxima a ser detectada
Fmax = 2000;
Fmin = 0;
% Batidas por minuto esperado para o audio
bpm = 196;
% Menor nota que deseja-se registrar
minNote = 1/4; % semicolcheia

%% Obtenção do Áudio
if(strcmp(import_audio_mode, 'local'))
    [audiofile, Fs, audioInfo] = getAudio(audio_name);
    
    T = 1/Fs;
    nBits = 24;
    nChannels = audioInfo.NumChannels;
    timeForRec = audioInfo.Duration;
    windowSize = round((minNote/2)*(60/bpm)*Fs);
    
    data = audiofile;
else
    Fs = 44100;
    T = 1/Fs;
    nBits = 24;
    nChannels = 2;
    windowSize = round((1/16)*(60/bpm)*Fs);
    recSize = Fs*timeForRec - mod(Fs*timeForRec, windowSize); % sample total that will be analysed
    
    recorder = audiorecorder(Fs,nBits,nChannels);
    
    disp('recording...');
    recordblocking(recorder, timeForRec);
    disp('recorded!');
    
    data = getaudiodata(recorder);
    
    audiowrite(audio_name, data, Fs);
end

%% Pré-Análise
audioLength = length(data);
spectrograma = zeros(Fmax*2, round(audioLength/windowSize));
pitch_results = zeros(1, round(audioLength/windowSize));

%% Análise
    disp('Analizing...');
    cont = 0;
    for i = 1 : windowSize : audioLength-windowSize
        cont = 1+cont;
        disp('teste');
        window = data(i:i+(windowSize-1), 1);
        [pitch, f, P] = detectPitch(window, Fs, nChannels);
        %disp(['janela[', int2str(cont), '] = ', int2str(size(window)), ' | t = ', num2str(i/Fs), ' | i = ', int2str(i), ' | pitch = ', num2str(pitch), ' | mean = ', num2str(mean(window))]);
        spectrograma(1:Fmax*2, cont) = P(1:Fmax*2);
        pitch_results(cont) = pitch;
    end
    disp('Analized!');

    sound(data, Fs, nBits);
    
    spectrograma = imresize(spectrograma,[4000 16000]);
    
    figure(1);
    imshow(spectrograma, []);
    
    figure(2);
    plot(pitch_results);
    
    figure(3);
    subplot(2, 1, 1);
    imshow(spectrograma, []);
    subplot(2, 1, 2);
    plot(pitch_results, '*');
    
    figure(4);
    spectrogram(data(:,1), ones(windowSize, 1), [], Fmin:Fmax, Fs, 'yaxis');