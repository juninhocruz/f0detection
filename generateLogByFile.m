function [t, f] = generateLogByFile(filename, T, duration)
    t = 0:T:duration-T;
    f = zeros(1, length(t));
    
    path = strcat('logs/', filename, '.log');
    fid = fopen(path);
    text = textscan(fid, '%s %d %f');
    notes = text{1};
    freq = text{2};
    time = text{3};
    acc = 1;
    for i=1:length(freq)-1
        lim = round(time(i+1)/T);
        f(acc:lim-1) = freq(i);
        acc = lim;
    end
    f(acc:end) = freq(end);
end