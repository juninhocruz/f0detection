function startAudioPlay(handles, guidata)
    %STARTAUDIOPLAY     StartAudioPlay - Plays the selected audio and plot 
    %                   simultaneously the signal and spectrum.

    %%                  Projeto Final de Curso - Sistema de Detecção de Pitch
    %
    %                   Universidade Federal do Amazonas
    %                   Faculdade de Tecnologia
    %                   Engenharia da Computação
    %                   Autor: António César Vieira da Cruz Júnior (CRUZ JR, A.C.V.)
    % 

    %% Configurações Iniciais Manuais
    % Path + Nome do audio a ser manipulado
    audio_name = strcat('samples/', handles.file);
    % Frequência Máxima a ser detectada
    Fmax = 2000;
    Fmin = 0;
    % Batidas por minuto esperado para o audio
    bpm = 196;
    % Menor nota que deseja-se registrar
    minNote = 1/4; % semicolcheia

    %% Obtenção do �?udio
    [audiofile, Fs, audioInfo] = getAudio(audio_name);

    T = 1/Fs;
    nBits = 24;
    nChannels = audioInfo.NumChannels;
    duration = audioInfo.Duration;
    windowSize = round((minNote/2)*(60/bpm)*Fs);

    data = audiofile;

    %% Pré-Análise
    audioLength = length(data);
    spectrograma = zeros(Fmax*2, round(audioLength/windowSize));
    pitch_results = zeros(1, round(audioLength/windowSize));

    %% Análise
    disp('Analizing...');
    cont = 0;
    for i = 1 : windowSize : audioLength-windowSize
        cont = 1+cont;
        disp(['time: ', num2str(i*T)]);
        window = data(i:i+(windowSize-1), 1);
        [pitch, ~, P] = detectPitch(window, Fs, nChannels);
        %disp(['janela[', int2str(cont), '] = ', int2str(size(window)), ' | t = ', num2str(i/Fs), ' | i = ', int2str(i), ' | pitch = ', num2str(pitch), ' | mean = ', num2str(mean(window))]);
        spectrograma(1:Fmax*2, cont) = P(1:Fmax*2);
        pitch_results(cont) = pitch;
    end
    disp('Analized!');

    sound(data, Fs, nBits);
    
    t = 0:T:duration;
    if(length(t) ~= length(data))
        t = 0:T:duration-T;
    end
    plot(handles.axes1, t, data);
    
    htoolbar = uitoolbar('Parent', ancestor(handles.axes4, 'figure'));
    uitoggletool('Parent', htoolbar, 'Tag', 'Exploration.Rotate', 'State', 'off');
    spectrogram(data(:,1), ones(windowSize, 1), [], Fmin:Fmax, Fs, 'yaxis');
    colorbar off;
    delete(htoolbar);
    
    figure(8);
    spectrogram(data(:,1), ones(windowSize, 1), [], Fmin:Fmax, Fs, 'yaxis');
    
    figure(7);
    plot(t, data);
    
    tp = 0:T*windowSize:duration-(T*windowSize);
    if(length(tp) ~= length(pitch_results))
        tp = 0:T*windowSize:duration;
    end
    plot(handles.axes3, tp, pitch_results, 'b.');
    
    figure(9);
    plot(tp, pitch_results, 'b.');
    
    [tlog, log] = generateLogByFile(handles.file, T, duration);
    plot(handles.axes2, tp, pitch_results, 'b.', tlog, log, 'r');
    
    figure(10);
    plot(tp, pitch_results, 'b.', tlog, log, 'r');
    
    disp(['Fs/windowSize: ', num2str(Fs/windowSize)]);
    
    
    spectYLimRef = handles.axes3.YLim;
    handles.axes1.XLim = [0 duration];
    handles.axes1.YLim = [min(data(:)) max(data(:))];
    handles.axes2.XLim = [0 duration];
    handles.axes2.YLim = spectYLimRef;
    handles.axes3.XLim = [0 duration];
    handles.axes3.YLim = spectYLimRef;
    handles.axes4.XLim = [0 duration];
    handles.axes4.YLim = spectYLimRef/1000;
    handles.axes4.YLabel.String = 'Frequency (Hz)';
    for i=1:length(handles.axes4.YTickLabel)
        handles.axes4.YTickLabel{i} = num2str(str2double(handles.axes4.YTickLabel{i})*1000);
    end
end