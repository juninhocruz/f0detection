function [audiofile, Fs, audioInfo] = getAudio(path_to_file)
%GETAUDIO   Get audio file and info by path to file
%   Usage: [audiofile, Fs, audioInfo] = getAudio(path_to_file)
%
%   Input parameters
%         path_to_file  : The path for access the audio file.
%
%   Output parameters:
%         audiofile     : Audio file with values of each sample
%         Fs            : Sample rate (in Hz)
%         audioInfo     : Info about the audio. (matlab audioinfo 
%   equivalent method) 
%
%   This function executes the matlab methods 'audioread' and 'audioinfo'
%   
%   Author: CRUZ JR, A.C.V.

    path = strcat(path_to_file);
    [audiofile, Fs] = audioread(path);
    audioInfo = audioinfo(path);
end