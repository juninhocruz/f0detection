function json = openjsonfile(filename)
    fid = fopen(filename); 
    raw = fread(fid,inf); 
    str = char(raw'); 
    fclose(fid); 
    json = jsondecode(str);
end