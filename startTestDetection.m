function startTestDetection(app)
    [audiofile, Fs, audioInfo] = getAudio('sample001.m4a');
    bpm = 196;
    minNote = 1/8; % semicolcheia
    
    T = 1/Fs;
    nBits = 24; %% ????
    nChannels = audioInfo.NumChannels;
    timeForRec = audioInfo.Duration;
    windowSize = round(minNote*(60/bpm)*Fs);
    recSize = Fs*timeForRec - mod(Fs*timeForRec, windowSize); % sample total that will be analysed
    
    setVal(app, 'Fs', Fs);
    setVal(app, 'T', T);
    setVal(app, 'nBits', nBits);
    setVal(app, 'nChannels', nChannels);
    setVal(app, 'timeForRec', timeForRec);
    setVal(app, 'bpm', bpm);
    setVal(app, 'windowSize', windowSize);
    setVal(app, 'recSize', recSize);
    
    data = audiofile;
    %spectrograma = zeros(size(data,1)/2+1, recSize/windowSize);
    %f = zeros(size(data,1)/2+1);
    
    %audiowrite('samples/sample008.m4a',data,Fs);
    
    disp('Analizing...');
    cont = 0;
    for i = 1 : windowSize : recSize
        cont = 1+cont;
        disp('teste');
        window = data(i:i+(windowSize-1), 1);
        [pitch, f, P] = detectPitch(window, Fs, nChannels);
        disp(['janela[', int2str(cont), '] = ', int2str(size(window)), ' | t = ', num2str(i/Fs), ' | i = ', int2str(i), ' | pitch = ', num2str(pitch), ' | mean = ', num2str(mean(window))]);
    %    spectrograma(:,((i-1)/windowSize)+1) = P';
    end
    disp('Analized!');

    sound(data, Fs, nBits);
end
