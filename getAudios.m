function audiosList = getAudios()
    filesList = dir('samples');
    listSize = size(filesList, 1);
    names = cell(1, listSize-2);
    for i=3:listSize
        names{i-2} = filesList(i).name;
    end
    audiosList = names;
end