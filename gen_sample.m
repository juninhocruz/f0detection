function generated_sample = gen_sample(timePerPitch, sample_rate, As, Fs)
    size = length(As);
    T = 1/sample_rate;
    t = (0:timePerPitch*sample_rate-1)*T;
    generated_sample = zeros(timePerPitch * sample_rate * size, 1);
    for i = 0 : size-1
        generated_sample(1+i*timePerPitch*sample_rate:i*timePerPitch*sample_rate+timePerPitch*sample_rate) = As(i+1) * sin(2*pi*Fs(i+1)*t);
    end
end