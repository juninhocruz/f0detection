function varargout = tccpitch(varargin)
% TCCPITCH MATLAB code for tccpitch.fig
%      TCCPITCH, by itself, creates a new TCCPITCH or raises the existing
%      singleton*.
%
%      H = TCCPITCH returns the handle to a new TCCPITCH or the handle to
%      the existing singleton*.
%
%      TCCPITCH('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TCCPITCH.M with the given input arguments.
%
%      TCCPITCH('Property','Value',...) creates a new TCCPITCH or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before tccpitch_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to tccpitch_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help tccpitch

% Last Modified by GUIDE v2.5 22-May-2018 21:38:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tccpitch_OpeningFcn, ...
                   'gui_OutputFcn',  @tccpitch_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before tccpitch is made visible.
function tccpitch_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tccpitch (see VARARGIN)

% Choose default command line output for tccpitch
handles.output = hObject;
handles.samples = handles.popupmenu.String;
handles.file = handles.samples{1};

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes tccpitch wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = tccpitch_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu.
function popupmenu_Callback(hObject, ~, handles) %#ok<DEFNU> Callback de criação do botão
% hObject    handle to popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu
handles.file = hObject.String{hObject.Value};

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popupmenu_CreateFcn(hObject, ~, ~) %#ok<DEFNU> Callback de criação do popup
% hObject    handle to popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
samples = getFiles('samples');
hObject.String = samples;

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button.
function button_Callback(~, ~, handles) %#ok<DEFNU> Callback do botão
% hObject    handle to button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startAudioPlay(handles, @guidata);
